package com.nut.er.controller.constants;

/**
 * @author shatrusudan
 */
public class EmployeeRegistrationPortletKeys {

	public static final String EmployeeRegistration = "employeeregistration";

}