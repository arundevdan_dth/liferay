<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />
	<style>
	.quick-links-container
	{
	padding: 0 !important;
	margin-top: 20px !important;
	}
	.row{
	margin: 0 !important;
}
.col-md-6.left-top-links-content {
		padding: 0 !important;
		margin-top: 20px !important;
	}
	.style_prevu_kit{
		height: 120px;
	}
	.style_prevu_kit img{
		width: 40px !important;
		margin: 0px;
	}
	.foot_logo{
		margin-right: -8px;
	}
	</style>
	
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div class="container-fluid" id="wrapper"">
	<header id="banner" role="banner">
		<div id="heading">
	<h1 class="site-title"  style="display:none;>
				<a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
					<img alt="${logo_description}" height="${site_logo_height}" src="${site_logo}" width="${site_logo_width}" />
				</a>

				<#if show_site_name>
					<span class="site-name" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
						${site_name}
					</span>
				</#if>
			</h1>		
		</div>
<div>
		<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />
  <@liferay_portlet["runtime"]
defaultPreferences="${freeMarkerPortletPreferences}"
portletProviderAction=portletProviderAction.VIEW
instanceId="ER_Banner"
portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet" />
${freeMarkerPortletPreferences.reset()}

		</div>

		<#if !is_signed_in>
			<a data-redirect="${is_login_redirect_required?string}" href="${sign_in_url}" id="sign-in" rel="nofollow">${sign_in_text}</a>
		</#if>

		<#if has_navigation && is_setup_complete>
			<#include "${full_templates_path}/navigation.ftl" />
		</#if>
	</header>

	<section id="content">
		<h1 class="hide-accessible">${the_title}</h1>
		
		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			
		</#if>
		
		
	</section>

	
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

<!-- inject:js -->
<!-- endinject -->

<!-- inject:js -->
<!-- endinject -->
<div id="footer"> 
<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />
  <@liferay_portlet["runtime"]
defaultPreferences="${freeMarkerPortletPreferences}"
portletProviderAction=portletProviderAction.VIEW
instanceId="ER_FOOTER"
portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet" />
${freeMarkerPortletPreferences.reset()}
</div>
</body>

</html>