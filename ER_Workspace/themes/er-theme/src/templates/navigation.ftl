

<nav class="${nav_css_class}" id="navigation" role="navigation">
	<h1 class="hide-accessible"><@liferay.language key="navigation" /></h1>

	<ul aria-label="<@liferay.language key="site-pages" />" role="menubar">
		<#list nav_items as nav_item>
			<#assign
				nav_item_attr_has_popup = ""
				nav_item_css_class = ""
				nav_item_layout = nav_item.getLayout()
			/>

			<#if nav_item.isSelected()>
				<#assign
					nav_item_attr_has_popup = "aria-haspopup='true'"
					nav_item_css_class = "selected"
				/>
			</#if>

			<li class="${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}" role="presentation">
				<a aria-labelledby="layout_${nav_item.getLayoutId()}" ${nav_item_attr_has_popup} href="${nav_item.getURL()}" ${nav_item.getTarget()} role="menuitem"><span><@liferay_theme["layout-icon"] layout=nav_item_layout /> ${nav_item.getName()}</span></a>

				<#if nav_item.hasChildren()>
					<ul class="child-menu" role="menu">
						<#list nav_item.getChildren() as nav_child>
							<#assign
								nav_child_css_class = ""
							/>

							<#if nav_item.isSelected()>
								<#assign
									nav_child_css_class = "selected"
								/>
							</#if>

							<li class="${nav_child_css_class}" id="layout_${nav_child.getLayoutId()}" role="presentation">
								<a aria-labelledby="layout_${nav_child.getLayoutId()}" href="${nav_child.getURL()}" ${nav_child.getTarget()} role="menuitem">${nav_child.getName()}</a>
							</li>
						</#list>
					</ul>
				</#if>
			</li>
		</#list>
	</ul>
</nav>

<head>   
<script src="js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="js/bootstrap.min.js"></script>
  <!-- All js -->
  <script src="js/dento.bundle.js"></script>
  <!-- Active js -->
  <script src="js/default-assets/active.js"></script>
  <script>
    function showSignInPopUP()
    {
     $("#signinpopupid").show();
    }
    function hideSignInPopUP()
    {
     $("#signinpopupid").hide();
 
    }
    $("#intro").click(function(event) {
      console.log($("#signinpopupid").find(event.target));
      
   if(document.getElementById("signinpopupid").style.display!="none" && event.target.id !="popup" && $("#signinpopupid").find(event.target).length == 0){
     $("#signinpopupid").hide();
   }
   
 });
 </script>
</head>
<body>
  <div class="top-header-area">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <!-- Top Content -->
          <div class="col-6 col-md-6 col-lg-6">
            <div class="top-header-content">
              <a href="#" data-toggle="tooltip" data-placement="bottom" title="28 Jackson Street, Chicago, 7788569 USA"><i class="fa fa-map-marker"></i> <span>28 Jackson Street, Chicago, 7788569 USA</span></a>
              <a href="#" data-toggle="tooltip" data-placement="bottom" title="info.dento@gmail.com"><i class="fa fa-envelope"></i> <span>info.dento@gmail.com</span></a>
            </div>
          </div>
          <!-- Top Header Social Info -->
          <div class="col-6 col-md-6 col-lg-6 socio-align">
            <div class="top-header-social-info text-right socio-align">
              <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" data-toggle="tooltip" data-placement="bottom" title="Google Plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin"></i></a>
              <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
            </div>
            
          </div>
        </div>
      </div>
    </div>

  <!-- ***** Header Area Start ***** -->
  <header class="header-area"> <!-- Main Header Start -->
    <div class="main-header-area">
      <div class="classy-nav-container breakpoint-off">
        <div class="container">
          <!-- Classy Menu -->
          <nav class="classy-navbar justify-content-between" id="dentoNav">

            <!-- Logo -->
            <a class="nav-brand" href="index.html">
              <img src="${images_folder}/er_common/core-img/logo.png" alt="">
            </a>

            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
              <span class="navbarToggler">
                <span></span>
                <span></span>
                <span></span>
              </span>
            </div>

            <!-- Menu -->
            <div class="classy-menu">

              <!-- Close Button -->
              <div class="classycloseIcon">
                <div class="cross-wrap">
                  <span class="top"></span>
                  <span class="bottom"></span>
                </div>
              </div>

              <!-- Nav Start -->
              <div class="classynav">
                <ul id="nav">
                  <li>
                    <a href="index.html">ABOUT US</a>
                  </li>
                  <li>
                    <a href="about.html">SERVICES</a>
                  </li>
                  <li>
                    <a href="service.html">BLOG</a>
                  </li>
                  <li>
                    <a href="pricing.html">CAREERS</a>
                  </li>
                  <li>
                    <a href="./contact.html">CONTACT US</a>
                  </li>
                  <li>
                    <li class="right loy"><a id="popup" onclick="showSignInPopUP()" class="trigger_popup_fricc pointer"><i class="ti-lock"></i>LOG IN<div class="ripple-container"></div></a> </li>
                  </li>
                  <li>
                    <div class="search-bar">
                      <input type="text" class="input" placeholder="&nbsp;">
                      <div class="search-btn">
                        <i class="fa fa-search" aria-hidden="true"></i>
                      </div>
                    </div>
                  </li>
                 
                </ul>
              </div>
              <!-- Nav End -->
            </div>
            <ul class="nav navbar-nav navbar-right float-right">
                  
              <li class="right login"><a id="popup" onclick="showSignInPopUP()" class="trigger_popup_fricc pointer"><i class="ti-lock"></i>LOG IN<div class="ripple-container"></div></a> </li>
            </ul>
          </nav>
          
          <div id="signinpopupid" class="account-popup-area signin-popup-box" style="display:none;" >
            <div class="account-popup" >
              <span   onclick="hideSignInPopUP()" class="close-popup pointer">&#10006<i class="la la-close "></i></span>
              <h3> Login</h3>
              
              
              <form>
                <div class="cfield">
                  <input type="text" placeholder="Username" required="true">
                  <i class="la la-user"></i>
                </div>
                <div class="cfield">
                  <input type="password" placeholder="********" required="true">
                  <i class="la la-key"></i>
                </div>
                <p class="remember-label">
                  <input class="checkbox" type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
                </p>
                <a href="#" title="">Forgot Password?</a>
                <button type="submit">Login</button>
              </form>
              <div class="extra-login">
               
                
              </div>
            </div>
          
        </div>
        </div>
      </div>
    </div>
  </header>
  
  
  
  
 
</body>